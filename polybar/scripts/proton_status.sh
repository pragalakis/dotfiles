#!/bin/sh

status=$(protonvpn-cli status | sed -n 's/Server: //p; s/Server Load: //p' | xargs)

if [[ $status == *"#"* ]];
then
  echo $status
else
  echo 'No VPN'
fi
