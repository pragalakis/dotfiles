return {
	"nvim-treesitter/nvim-treesitter",
	build = function()
		require("nvim-treesitter.install").update({
			with_sync = true,
			indent = { enable = true },
			highlight = { enable = true },
			ensure_installed = {
				"lua",
				"vim",
				"vimdoc",
				"javascript",
				"html",
				"css",
				"bash",
				"json",
				"python",
				"sql",
				"typescript",
				"xml",
				"markdown",
				"yaml",
				"tsx",
			},
		})()
	end,
}
