export PATH=~/.npm-global/bin:$PATH

# gnome-keyring-daemon for ssh in i3
if [ "$0" = "/etc/gdm/Xsession" -a "$DESKTOP_SESSION" = "i3" ]; then
    export $(gnome-keyring-daemon --start)
    # SSH_AGENT_PID required to stop xinitrc-common from starting ssh-agent
    export SSH_AGENT_PID=${GNOME_KEYRING_PID:-gnome}
fi


# Android Debug Bridge 
if [ -d "$HOME/Downloads/adb-fastboot/platform-tools" ] ; then
 export PATH="$HOME/Downloads/adb-fastboot/platform-tools:$PATH"
fi
