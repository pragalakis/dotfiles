" Plugins will be downloaded under the specified directory.
call plug#begin('~/.vim/plugged')

" Declare the list of plugins.
Plug 'dracula/vim', { 'as': 'dracula' }
Plug 'scrooloose/nerdtree'
Plug 'vim-airline/vim-airline'
Plug 'pangloss/vim-javascript'
Plug 'mxw/vim-jsx'
Plug 'gorodinskiy/vim-coloresque'
Plug 'ervandew/supertab'
Plug 'matze/vim-move'
Plug 'townk/vim-autoclose'
Plug 'sbdchd/neoformat'
Plug 'prettier/vim-prettier'
Plug 'w0rp/ale'
Plug 'tpope/vim-surround'
Plug 'jistr/vim-nerdtree-tabs'
Plug 'Valloric/YouCompleteMe'
Plug 'kien/ctrlp.vim'
Plug 'sirver/ultisnips'
Plug 'tpope/vim-fugitive'
Plug 'leafgarland/typescript-vim'

" List ends here. Plugins become visible to Vim after this call.
call plug#end()

" Fix the background color issue
let g:dracula_colorterm=0

" Activate dracula theme
syntax on
color dracula

" Set Dracula colors instead of terminal
set termguicolors

" vim-airline Tab Line
let g:airline#extensions#tabline#enabled=1

" Ctrl + n for TreeMap
map <C-n> :NERDTreeToggle<CR>

" 256 color support for rxvt
set t_Co=256

set number
set encoding=UTF-8
set smartindent
set tabstop=2
set shiftwidth=2
set expandtab

" Mouse
"set mouse=a  " Enable mouse use in all modes

" Turn Off Swap Files
set noswapfile                " Disable .swp files
set nobackup                  " Disable ~ backup files
set nowritebackup             " No really
set backupdir=/var/tmp,/tmp   " But if you do, write it here
set directory=/var/tmp,/tmp   " Or here

" Add new tab like Firefox.
nnoremap <C-t>     :tabnew<CR>
inoremap <C-t>     <Esc>:tabnew<CR>

" Select all text
"nmap <C-a> ggVG

" Allow C-k and C-j for move actions
let g:move_key_modifier = 'C'
let g:move_key_modifier_visualmode = 'C'

" Fix backspace behavior
set backspace=indent,eol,start

" Keep Undo history on buffer change
set hidden

" Make Neoformat run on save 
autocmd BufWritePre *.js Neoformat

" Async prettier 
let g:prettier#exec_cmd_async=1

" Run prettier before saving async 
let g:prettier#autoformat = 0
let g:prettier#autoformat_require_pragma = 0
autocmd BufWritePre *.js,*.css,*.scss,*.less,*jsx,*ts,*tsx,*.mjs,*markdown,*md,*yaml,*html PrettierAsync

" Enable the prettier fixer on ale 
let g:ale_fixers = {'javascript': ['prettier','eslint'],'typescript': ['prettier'],'scss': ['prettier'],'html': ['prettier']}
let g:ale_fix_on_save = 1

" Enable single quote on prettier
let g:prettier#config#single_quote = 'true'

" Enable semicolon on prettier
let g:prettier#config#semi = 'true'

" Remove trailing commas on prettier
let g:prettier#config#trailing_comma = 'none'

" max line length that prettier will wrap on
let g:prettier#config#print_width = 80

" Start autocompletion after 4 chars
let g:ycm_min_num_of_chars_for_completion = 4
let g:ycm_min_num_identifier_candidate_chars = 4
let g:ycm_enable_diagnostic_highlighting = 0

" Don't show YCM's preview window
set completeopt-=preview
let g:ycm_add_preview_to_completeopt = 0

" Trigger snippet 
let g:UltiSnipsExpandTrigger="<c-space>"
