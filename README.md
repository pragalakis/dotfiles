# [deprecated] dotfiles

Up to date repo on: https://codeberg.org/evasync/dotfiles

Personal dotfiles:

- .bashrc
- .aliases
- bash_profile
- profile
- dunst
- i3
- waybar
- swaylock
- sddm
- prettierrc
- vim & nvim
- Ultisnips
- sway (wayland alternative to i3)
- wlsunset (wayland alternative to redshift)
- protonvpn service
- MAC randomizer service
- DNS over TLS service
- [/etc/hosts](https://github.com/StevenBlack/hosts)
- ublock static filters (blocking google and youtube popups)
- xfce4 (thunar)

### Archived

- .Xresources and .xinitrc
- tern-config
- i3status-rs (as an alternative to polybar)
- i3status (as an alternative to polybar)
- polybar (status bar for i3wm) + scripts
- redshift service (does not work in wayland)
- i3

### Dracula Theme - i3, polybar, dmenu

![](screenshot-2.png)

You can generate the wallpaper with [canvas-sketch](https://gitlab.com/pragalakis/canvas-playground/blob/master/random/i3-dracula.js)
